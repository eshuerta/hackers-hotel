import React, {Component} from 'react';
import Bookings from './components/Bookings';
import Meals from './components/Meals';
import Error from './components/Error';


class App extends Component {

    state = {
        meals: [],
        errors: [],
    }

    getMealsSchedule = (data) => {
        this.setState({ meals: data.meals, errors: data.errors } ); 
    }

    render() {
      
        return (<div className="container-fluid">
            <center>
                <h2>Hacker Hostel</h2>
            </center>
            <div className="container">
                <Bookings getMealsSchedule={this.getMealsSchedule}></Bookings>
                <Error errors={this.state.errors || []}></Error>
                <Meals meals={this.state.meals || []}></Meals>
            </div>
        </div>);
    }
}

export default App;