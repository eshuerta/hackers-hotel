import React, {Component} from 'react';
import {PropTypes} from 'prop-types';

const Meals = ((props) => {
    return (
        <div className="col-xs-12  col-sm-12 col-md-12 col-lg-12">
            <ol id="list">
                <div>
                    {props && props.meals && props.meals.map((meal, index) => 
                        <li key={meal.hackerName+index} className={meal.class}>{meal.type} for {meal.hackerName} on {meal.date}</li>                   
                    )}   
                </div> 
            </ol>
        </div>);
});
export default Meals;
