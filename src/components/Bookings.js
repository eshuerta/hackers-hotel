import React, {Component} from 'react';
import {PropTypes} from 'prop-types';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

class Bookings extends Component {

  state = {
    hackers: '',
    dates: '',
    errors: [],
  }

  handleChangeHacker = (event) => {
    this.setState({ hackers: event.target.value });
  };

  handleChangeDate = (event) => {
    this.setState({ dates: event.target.value });
  };

  handleGuestInfo = (obj) => {
    this.setState({ hackers: obj.target.value });
  }

  handleDateInfo = (obj) => {
    this.setState({ dates: obj.target.value });
  }
  
  // Diferencia en dias entre dos fechas
  getDateDiff = (minimumDate, maximumDate) => {
    let diffTime = maximumDate.getTime() - minimumDate.getTime(); 
    return diffTime / (1000 * 3600 * 24); 
  }

  // Formato de fecha de salida
  getFormattedDate = date => {
    let nDate = new Date(date);
    let dd = nDate.getDate();
    let mm = nDate.getMonth() + 1; 
    let yyyy = nDate.getFullYear();

    if (dd < 10) {
      dd = '0' + dd;
    }

    if (mm < 10) {
      mm = '0' + mm;
    }

    return `${yyyy}-${mm}-${dd}`;
  }

  // Obtiene la menor y la mayor fecha de todas las ingresadas y valida formatos varios
  getMinAndMaxDates = (dates, hackers) => {
    const datesList = [];
    const errors = [];
    hackers.map( (hackerName, index) => {
      // Formato valido?
      if (this.isValidDateFormat(dates[index])) {
        // obtiene las dos fechas a partir del string ingresado de fechas
        const ds = this.splitDateAndCreateObject(dates[index]);
        // Push al array la fecha desde y hasta de cada hacker
        datesList.push(ds.d1, ds.d2);
      } else {
        // Si hubo un problema con la fecha agrego el nombre del hacker a la lista de erorres
        errors.push(hackers[index]);
      }      
    });
    
    // Fecha maxima y minima del array de fechas
    const maximumDate=new Date(Math.max.apply(null, datesList)); 
    const minimumDate=new Date(Math.min.apply(null, datesList)); 
    return {minimumDate, maximumDate, errors};    
  }

  /*
   * Chequea:
   * si el formato de fecha es valido "yyyy-mm-dd to yyyy-mm-dd"
   * Si las fechas son validas 
   * Si la fecha hasta es mayor que la fecha desde
  */
  isValidDateFormat = (dateString) => {
    
    if (dateString) {
      // yyyy-mm-dd to yyyy-mm-dd
      var regEx = /^\d{4}-\d{2}-\d{2}\b.to.\d{4}-\d{2}-\d{2}$/;
      if (dateString.match(regEx)) {      
        const ds = this.splitDateAndCreateObject(dateString);
        // Fecha 2 es menor?
        if (ds.d2 < ds.d1) {
          return false;
        } else { // formato de fechas validas ?
          return ds.d1 instanceof Date && !isNaN(ds.d1) && ds.d2 instanceof Date && !isNaN(ds.d2);
        }
      }
      return false;
    } else {
      return false;
    }   
  }

  // Split del string y obtencion de objeto Date
  splitDateAndCreateObject = dateString => {
    if (dateString) {
      let dateSplit = dateString.split(" to ");       
      var d1 = new Date(dateSplit[0]);
      var d2 = new Date(dateSplit[1]);
      return {d1, d2};
    } else {
      return {d1: '', d2: ''};
    }
  }

 
  // Handler click "get meals schedule"
  getMealsSchedule = () => {
    let { hackers, dates } = this.state;

    let hackersSplit = hackers.split('\n');
    let datesSplit = dates.split('\n');
    
    // Obtiene la menor y la mayor fecha de todas las fechas ingresadas y valida formato
    const validMinMaxDatesAndErrors = this.getMinAndMaxDates(datesSplit, hackersSplit);
    // Obtiene la diferencia de dias entre las dos fechas
    const datesAndDiff = this.getDateDiff(validMinMaxDatesAndErrors.minimumDate, validMinMaxDatesAndErrors.maximumDate);    
    
    let breakfasts = [];
    let lunchs = [];
    let dinners = [];
    let errors = validMinMaxDatesAndErrors.errors;

    // dia por dia de la fecha minima ingresada a la maxima
    for (let i = 0; i <= datesAndDiff; i++) {
      // Obtengo la menor fecha
      let nDate = new Date(validMinMaxDatesAndErrors.minimumDate);
      // Sumo i dias a la menor fecha
      nDate.setDate(nDate.getDate() + i);
      
      // Reviso que cada hacker este dentro de la fecha actual
      hackersSplit.map( (hackerName, index) => {
        const hackerDates = this.splitDateAndCreateObject(datesSplit[index]);
        
        if (hackerDates && hackerDates.d1 <= nDate && hackerDates.d2 >= nDate) {
          const dFormatted = this.getFormattedDate(nDate);
          breakfasts.push({
            type: "Breakfast",
            hackerName,
            class: "morning",
            date: dFormatted
          });

          lunchs.push({
            type: "Lunch",
            hackerName,
            class: "afternoon",
            date: dFormatted
          });

          dinners.push({
            type: "Dinner",
            hackerName,
            class: "night",
            date: dFormatted
          });
        }
      });

    }

    // Uno todos los arrays y mando al componente padre
    this.props.getMealsSchedule({ meals: breakfasts.concat(lunchs, dinners), errors });

  }

  

  render() {
    return (
      <div className="row">
        <TextField
          className="col-md-6"
          multiline
          rows="4"
          placeholder="Enter the hacker list (one hacker per line)"
          onChange={this.handleChangeHacker}
          value={this.state.hackers}
        />
        <TextField
          className="col-md-6"
          multiline
          rows="4"
          placeholder="Enter the date range for each hacker's stay (one range per line)"
          onChange={this.handleChangeDate}
          value={this.state.dates}
        />
        <Button variant="outlined" color="primary" className="block-center" onClick={this.getMealsSchedule}>Get Meals Schedule</Button>
      </div>);
  }
}

export default Bookings;